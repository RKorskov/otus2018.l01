-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
-*- eval: (set-language-environment Russian) -*-  
--- Time-stamp: <2019-07-17 18:47:14 korskov>

Overview
========

otus-java_2018-12

Группа 2018-12  
Студент:  
[Roman Korskov (Роман Корсков)](https://gitlab.com/RKorskov/otus-java_2018-12)  
korskov.r.v@gmail.com  

[Разработчик Java  
Курс об особенностях языка и платформы Java, стандартной библиотеке, о
проектировании и тестировании, о том, как работать с базами, файлами,
веб-фронтендом и другими
приложениями](https://otus.ru/lessons/razrabotchik-java/)

Домашние Работы
===============

# L1 Сборка и запуск проекта #

Создать проект под управлением maven в Intellij IDEA.

Добавить зависимость на Google Guava/Apache Commons/библиотеку на ваш выбор.  
Использовать библиотечные классы для обработки входных данных.

Задать имя проекта (`project_name`) в pom.xml  
Собрать project_name.jar содержащий все зависимости.  
Проверить, что приложение можно запустить из командной строки.

Выложить проект на github.

Создать ветку "obfuscation" изменить в ней pom.xml, так чтобы сборка
содержала стадию обфускации байткода.

## TODO ##

Use: Netty 4.1.37

L2 network address echo reporter:
```java
URL autoIp = new URL("http://ip1.dynupdate.no-ip.com:8245/");
InputStreamReader isr = new InputStreamReader(autoIp.openStream());
BufferedReader in = new BufferedReader(isr);
externalIp = in.readLine();
```

... which returns something like this:
```html
<html>
  <head></head>
  <body>
    62.152.87.202
  </body>
</html>
```

Also, `curl -D - ip1.dynupdate.no-ip.com:8245`

## to read ##

1. [Как перестать бояться Proguard и начать жить](https://habr.com/ru/post/415499/)
2. [Netty User guide for 4.x](https://netty.io/wiki/user-guide-for-4.x.html)
3. [Jenkov Netty TCP Server](http://tutorials.jenkov.com/netty/netty-tcp-server.html)
