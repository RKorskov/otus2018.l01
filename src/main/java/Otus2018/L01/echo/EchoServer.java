// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2019-07-17 16:18:43 korskov>

package L01.echo;

import io.netty.bootstrap.ServerBootstrap;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
    
/**
 * Let us learn how to write a response message to a client by
 * implementing the ECHO protocol, where any received data is sent
 * back.
 *
 * The only difference from the discard server we have implemented in
 * the previous sections is that it sends the received data back
 * instead of printing the received data out to the console.
 *
 * https://netty.io/wiki/user-guide-for-4.x.html
 */

public class EchoServer {
    
    private int port;
    
    public EchoServer(int port) {
        this.port = port;
    }
    
    public void run() throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) //
                            throws Exception {
                            ch.pipeline().addLast(new EchoServerHandler());
                        }
                    })
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_KEEPALIVE, true);
    
            // Bind and start to accept incoming connections.
            ChannelFuture f = b.bind(port).sync(); // (7)
    
            // Wait until the server socket is closed.
            // In this example, this does not happen, but you can do
            // that to gracefully shut down your server.
            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
    
    public static void main(final String[] args) throws Exception {
        int port = 58247;
        if (args != null && args.length > 0) {
            port = Integer.parseInt(args[0]);
        }
        System.out.printf("echo server on port %d\n", port);
        new EchoServer(port).run();
    }
}
