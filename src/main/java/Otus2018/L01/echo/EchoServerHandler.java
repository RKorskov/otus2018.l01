// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2019-07-17 16:40:42 korskov>

package L01.echo;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * Handles a server-side channel.
 *
 * https://netty.io/wiki/user-guide-for-4.x.html
 */

public class EchoServerHandler extends ChannelInboundHandlerAdapter { // (1)

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        /**
         * 1. A ChannelHandlerContext object provides various operations
         * that enable you to trigger various I/O events and
         * operations. Here, we invoke write(Object) to write the received
         * message in verbatim. Please note that we did not release the
         * received message unlike we did in the DISCARD example. It is
         * because Netty releases it for you when it is written out to the
         * wire
         */
        ctx.write(msg); // (1)
        /**
         * 2. ctx.write(Object) does not make the message written out
         * to the wire. It is buffered internally and then flushed out
         * to the wire by ctx.flush(). Alternatively, you could call
         * ctx.writeAndFlush(msg) for brevity
         */
        ctx.flush(); // (2)
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
