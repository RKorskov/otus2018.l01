// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2019-07-17 16:52:41 korskov>

package Otus2018.L01;

import L01.discard.DiscardServer;
import L01.echo.EchoServer;
import L01.time.TimeServer;
import L01.dynupdate.DynUpdateServer;

/**
 * mvn exec:java -Dexec.args="discard"
 */

public class ServerSelect {
    public static void main(final String[] args) {
        try {
            for(String s : args)
                switch(s.toUpperCase()) {
                case "DISCARD":
                    DiscardServer.main(null);
                    break;
                case "ECHO":
                    EchoServer.main(null);
                case "TIME":
                    TimeServer.main(null);
                case "DYNUPDATE":
                    DynUpdateServer.main(null);
                case "-H":
                case "-HELP":
                case "--HELP":
                default:
                    System.out.printf("Available protocols:\n"
                                      + "discard \t 58246\n"
                                      + "echo \t\t 58247\n"
                                      + "time \t\t 58248\n"
                                      + "dynupdate \t 8245\n"
                                      );
                    return;
                }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
