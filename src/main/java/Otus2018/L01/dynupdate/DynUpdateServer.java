// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2019-07-17 18:50:13 korskov>

package L01.dynupdate;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * L2 network address echo reporter:
 ```java
 URL autoIp = new URL("http://ip1.dynupdate.no-ip.com:8245/");
 InputStreamReader isr = new InputStreamReader(autoIp.openStream());
 BufferedReader in = new BufferedReader(isr);
 externalIp = in.readLine();
 ```

 ... which returns something like this:
 ```
 HTTP/1.0 200 OK
 Server: xipha
 Date: Thu, 17 Jul 2019 15:55:42 GMT
 Content-Type: text/html
 Connection: close

 62.152.87.202
 ```
 */

public class DynUpdateServer {
    private final static int __DEFAULT_PORT = 8245;
    private final static String __DEFAULT_HOST = "localhost";
    private int port;
    private String host;

    public DynUpdateServer() {
        this(__DEFAULT_HOST, __DEFAULT_PORT);
    }

    public DynUpdateServer(String host) {
        this(host, __DEFAULT_PORT);
    }

    public DynUpdateServer(int port) {
        this(__DEFAULT_HOST, port);
    }

    public DynUpdateServer(String host, int port) {
        this.port = port;
        this.host = host;
    }

    public void run() throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) //
                            throws Exception {
                            ch.pipeline().addLast(new DynUpdateServerHandler());
                        }
                    })
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_KEEPALIVE, true);
    
            // Bind and start to accept incoming connections.
            ChannelFuture f = b.bind(port).sync();
    
            // Wait until the server socket is closed.
            // In this example, this does not happen, but you can do
            // that to gracefully shut down your server.
            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    public static void main(final String[] args) throws Exception {
        int port = __DEFAULT_PORT;
        if (args != null && args.length > 0) {
            port = Integer.parseInt(args[0]);
        }
        System.out.printf("dynupdate server on port %d\n", port);
        new DynUpdateServer(port).run();
    }
}
