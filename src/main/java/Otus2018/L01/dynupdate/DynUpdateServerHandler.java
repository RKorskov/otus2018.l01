// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2019-07-18 12:45:10 korskov>

package L01.dynupdate;

import java.lang.CharSequence;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.net.SocketAddress;
import java.net.InetSocketAddress;
import java.net.InetAddress;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

public class DynUpdateServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        String addr = "0.0.0.0/0";
        try {
            Channel ch = ctx.channel();
            SocketAddress sa = ch.remoteAddress();
            if (sa instanceof InetSocketAddress) {
                InetSocketAddress isa = (InetSocketAddress) sa;
                InetAddress ia = isa.getAddress();
                addr = ia.getHostAddress();
            }
            else
                addr = sa.toString();
        }
        catch (NullPointerException npe) {
            npe.printStackTrace();
            //addr = "0.0.0.0/0";
        }
        final String msg = "HTTP/1.0 200 OK\r\n"
            + "Host: attohost\r\n"
            + "Content-Type: text/html\r\n"
            + "Connection: close\r\n"
            + "\r\n"
            + addr;
        CharSequence cmsg = msg;
        final ByteBuf time = ctx.alloc().buffer(cmsg.length()); // (2)
        time.writeCharSequence(cmsg, StandardCharsets.US_ASCII);
        final ChannelFuture f = ctx.writeAndFlush(time); // (3)
        f.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) {
                assert f == future;
                ctx.close();
            }
        });
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
