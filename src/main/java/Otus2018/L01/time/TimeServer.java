// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2019-07-17 16:41:48 korskov>

package L01.time;

import io.netty.bootstrap.ServerBootstrap;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
    
/**
 * The protocol to implement in this section is the TIME protocol. It
 * is different from the previous examples in that it sends a message,
 * which contains a 32-bit integer, without receiving any requests and
 * closes the connection once the message is sent. In this example,
 * you will learn how to construct and send a message, and to close
 * the connection on completion.
 *
 * https://netty.io/wiki/user-guide-for-4.x.html
 */

public class TimeServer {
    
    private int port;
    
    public TimeServer(int port) {
        this.port = port;
    }

    public void run() throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) //
                            throws Exception {
                            ch.pipeline().addLast(new TimeServerHandler());
                        }
                    })
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_KEEPALIVE, true);
    
            // Bind and start to accept incoming connections.
            ChannelFuture f = b.bind(port).sync();
    
            // Wait until the server socket is closed.
            // In this example, this does not happen, but you can do
            // that to gracefully shut down your server.
            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    public static void main(final String[] args) throws Exception {
        int port = 8245;
        if (args != null && args.length > 0) {
            port = Integer.parseInt(args[0]);
        }
        new TimeServer(port).run();
    }
}
